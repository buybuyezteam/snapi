'use strict';

angular.module('starter.services', ['ngResource'])

.factory('Login', function($resource, ENV){
  return $resource(ENV.loginEndpoint ,{},{
    logout: {
      method: 'POST',
      url: ENV.loginEndpoint + 'logout'
    },
    login: {
      method: 'POST',
      url: ENV.loginEndpoint + 'login'
    }
  });
})

.factory('Users', function($resource, ENV){
  return $resource(ENV.loginEndpoint + 'usuarios');
})

.factory('Lista', function($resource, ENV){
	return $resource(ENV.firebaseAPI + 'listas/:usuario.json', {usuario:'@usuario'});
})

.factory('Productos', function($resource, ENV){
	return $resource(ENV.firebaseAPI + 'productos/:productoCodigo.json', {productoCodigo:'@productoCodigo'});
})

.factory('ProductosAPI', function($resource, ENV){
  return $resource(ENV.apiEndpoint + 'producto/:productoCodigo', {productoCodigo:'@productoCodigo'});
})

.factory('ProductosT', function($resource, ENV){
  return $resource(ENV.apiEndpoint + 'productosT/:tiendaId', {tiendaId:'@tiendaId'});
})

.factory('Carritos', function($resource, ENV){
  return $resource(ENV.apiEndpoint + 'guardarCarrito');
})

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  };
}])

.factory('Camera',['$q',function($q){
  return {
    getPicture: function(options){
      var q = new $q.defer();
      navigator.camera.getPicture(function(result){
        q.resolve(result);
      }, function(err){
        q.reject(err);
      }, options);

      return q.promise;
    }
  };
}])

.directive('match', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'A',
        link: function(scope, elem, attrs, ctrl) {
            function getMatchValue(){
                var match = matchGetter(scope);
                if(angular.isObject(match) && match.hasOwnProperty('$viewValue')){
                    match = match.$viewValue;
                }
                return match;
            }

            if(!ctrl) {
                if(console && console.warn){
                    console.warn('Match validation requires ngModel to be on the element');
                }
                return;
            }

            var matchGetter = $parse(attrs.match);

            scope.$watch(getMatchValue, function(){
                ctrl.$$parseAndValidate();
            });

            ctrl.$validators.match = function(){
                return ctrl.$viewValue === getMatchValue();
            };

            
        }
    };
})
;