'use strict';

angular.module('starter.controllers', ['firebase'])

.controller('LogoutCtrl', function($scope, auth, Login, $ionicPopup, $location){
  $scope.logout = function (){
    Login.logout().$promise
    .then(function(){
      $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Aviso',
          template: 'Usted ha salido del sistema'
        });
        alertPopup.then(function() {
          $location.path('app/activartienda')  ;
        });
      };
      
      $scope.showAlert();
      auth.signout();
    });
  };
  $scope.logout();
})

.controller('LoginCtrl', function($scope, auth, $state, $location, $localstorage, $ionicModal, Users, Login, $ionicLoading, $ionicPopup){
  $scope.registro = {genero:{}};
  $scope.loginData = {};
  $scope.generos = [
    {id:'M', nombre:'Masculino'},
    {id:'F', nombre:'Femenino'},
    {id:'O', nombre:'Otro'}
  ];

  $scope.registrar = function(){
    $ionicLoading.show();
    var usuario = new Users();
    usuario.name = $scope.registro.name;
    usuario.cedula = $scope.registro.cedula;
    usuario.rif = $scope.registro.rif;
    usuario.email = $scope.registro.email;
    usuario.password = $scope.registro.password;
    usuario.direccion = $scope.registro.direccion;
    usuario.dob = $scope.registro.dob;
    usuario.sexo = $scope.registro.genero.id;
    usuario.$save().finally(function(){
      $ionicLoading.hide();
    });
    
  };

  $scope.loguear = function(){
    
    Login.login($scope.loginData).$promise
    .then(function(data){
      if (data.data.status === true){
        $localstorage.setObject('snapConfig', data.data.usuario);
        auth.isAuthenticated = data.data.status;
        $scope.loginData.email = '';
        $scope.loginData.password = '';

        $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Aviso',
          template: 'Usted ha ingresado al sistema'
        });
        alertPopup.then(function() {
          $location.path('app/activartienda')  ;
        });
      };
      
      $scope.showAlert();

        //$location.path('app/activartienda');
      }else{
        $scope.showAlert = function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Error',
            template: data.data.message
          });
          alertPopup.then(function() {
            console.log('Thank you for not eating my delicious ice cream cone');
          });
        };
        
        $scope.showAlert();
        
      }
      
    });
  };

  $scope.doLogout = function(){
    auth.signout();
  };

  $scope.logout = function (){
    Login.logout().$promise
    .then(function(){
      $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Usted ha salido del sistema'
        });
      };
      
      $scope.showAlert();
    });
  };

  $scope.doLogin = function(){
    auth.signin({
      socialBigButtons: true,
      dict: 'es'
    }, function(profile, idToken){
      $scope.profile = profile;
      $scope.idToken = idToken;
      $location.path('app/activartienda');
    }, function(err){

    });
  };

  $scope.message = {};

  $scope.doGoogleLogin = function () {
    $scope.message.text = 'loading...';
    $scope.loading = true;

    auth.signin({
      popup: true,
      socialBigButtons: true,
      connection: 'google-oauth2',
      scope: 'openid name email'
    }, function(profile, token){
      $location.path('app/activartienda');
    }, function(){});
  };

  $ionicModal.fromTemplateUrl('registro-modal.html', {
    id: 1, // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function(){
    $scope.modal.show();
  };

  $scope.closeModal = function(){
   $scope.modal.hide(); 
  };
})

.controller('TermsCtrl', function($scope, $localstorage){
  var config = $localstorage.getObject('snapConfig');
  if (config !== undefined){
    
  }
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, Productos, $localstorage,$ionicLoading, ProductosT, ENV, $resource, $state, auth, $location, Login, $ionicPopup) {
  // Form data for the login modal
  $scope.loginData = {};
  $scope.tienda = {};
  $scope.dataDeProductos = 0;

  $scope.logout = function (){
    Login.logout().$promise
    .then(function(){
      $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Aviso',
          template: 'Usted ha salido del sistema'
        });
        alertPopup.then(function() {
          $location.path('home/home-login')  ;
        });
      };
      
      $scope.showAlert();
      auth.signout();
    });
  };

  $scope.scanTiendaCamera = function(){
    cordova.plugins.barcodeScanner.scan(function(data){      

      var decrypted = CryptoJS.AES.decrypt(data.text, 'snapp es lo mejor');
      $scope.msg = decrypted.toString(CryptoJS.enc.Utf8);
      $localstorage.set('apiEndpoint', $scope.msg);
      ENV.apiEndpoint = $scope.msg;

      var Activacion = $resource($scope.msg + 'activacion');

      Activacion.get().$promise
      .then(function(data){
        $scope.tienda = data.tienda;
        $localstorage.setObject('tienda', data.tienda);
      });

      $ionicLoading.show();

      var Productitos = $resource($scope.msg + 'productosT/:tiendaId', {tiendaId:'@tiendaId'});

      Productitos.get({tiendaId:1}).$promise
      .then(function(data){
        $localstorage.setObject('productos', data.toJSON());
        $scope.dataDeProductos = 1;
        $state.go('app.comprafire2');
        
      })
      .catch(function(error){
        console.log(error);
      })
      .finally(function(){
        $ionicLoading.hide();
      });

    });

  };
  //console.log(CryptoJS.AES.encrypt('http://192.168.1.100:8000/api/v1/', 'snapp es lo mejor').toString());

  $scope.scanTienda = function(id){
    var data = '';
    if (id===1)   {
      data = {text:'U2FsdGVkX18Q31KkRP0BoVHVbTikbU3by/zvSnbSAZURYvNeKdr0MGqqzgBxtePkppjeWdX2EXzi0s0PBt/Q+Q=='};
      $scope.msg = 'http://192.168.1.101:8000/api/v1/';
    }else{
      data = {text:'U2FsdGVkX1+BvEotaqwqT39ZopdsVm0AfF2Disq3qp2b0UtOvT2Z7HzwS3daGcVYRqSo1MT41Goo85P5ZUERIg=='};
      var decrypted = CryptoJS.AES.decrypt(data.text, 'snapp es lo mejor');
      $scope.msg = decrypted.toString(CryptoJS.enc.Utf8);
    }
    
    ENV.apiEndpoint = $scope.msg;
    $localstorage.set('apiEndpoint', $scope.msg);

    var Activacion = $resource($scope.msg + 'activacion');

    Activacion.get().$promise
    .then(function(data){
      $scope.tienda = data.tienda;
      $localstorage.setObject('tienda', data.tienda);
    });

    $ionicLoading.show();
    var Productitos = $resource($scope.msg + 'productosT/:tiendaId', {tiendaId:'@tiendaId'});
    Productitos.get({tiendaId:1}).$promise
    .then(function(data){
      $localstorage.setObject('productos', data.toJSON());
      $state.go('app.comprafire2');
    })
    .catch(function(error){
      console.log(error);
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  };

  var config = $localstorage.getObject('snapConfig');
  if (config !== undefined){
    
  }
  
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    id:1
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $ionicModal.fromTemplateUrl('templates/common/terms.html', {
    id: '2',
    scope: $scope,
    backdropClickToClose: false,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.oModal2 = modal;
  });

  $scope.openTerms = function(){
    $scope.oModal2.show();
  };

  $scope.aceptar = function(){
   $scope.oModal2.hide(); 
  };

  $scope.doLogout = function(){
    auth.signout();
    $state.go('home.login');
  };

})

.controller('ListaCtrl', function($scope, Lista) {
  $scope.itemLista = {};

  Lista.get({usuario:11738998}, function(res){
    $scope.lista = res;
  });

  $scope.guardar = function(){
    $scope.lista.push({producto:$scope.itemLista.producto, cantidad:$scope.itemLista.cantidad});

  };
})

.controller('ListafireCtrl', function($scope, $firebaseArray, Firebase, ENV, $localstorage, $ionicModal, Camera){
  var lista = new Firebase(ENV.firebaseAPI + 'listas');
  var config = $localstorage.getObject('snapConfig');
  $scope.producto = {imagen:'', nombre:'', cantidad:''};

  $scope.takePicture = function(){
    
    navigator.camera.getPicture(
      function(imageData){
        $scope.producto.imagen = imageData;
        $scope.apply();
      }, 
      function(error){console.log(error);},
      { 
        quality: 30,
        targetWidth: 150,
        targetHeight: 150,
        destinationType: Camera.DestinationType.DATA_URL,
        encodingType: Camera.EncodingType.JPEG,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : false
      }
    );
  };

  $scope.lista = $firebaseArray(lista.child(config.cedula));

  $ionicModal.fromTemplateUrl('my-modal.html', {
    id: 1, // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function(){
    $scope.modal.show();
  };

  $scope.closeModal = function(){
   $scope.modal.hide(); 
  };

  $scope.guardar = function(){
    $scope.lista.$add({
      nombre: $scope.producto.nombre,
      cantidad: $scope.producto.cantidad,
      imagen: $scope.producto.imagen
    });
    $scope.producto = {imagen:'', nombre:'', cantidad:''};
    $scope.closeModal();
  };

  $scope.update = function(){
    lista.child(config.cedula)
    .child($scope.producto.$id)
    .update({cantidad:$scope.producto.cantidad});
    $scope.closeModal();
  };

  $scope.edit = function(item){
    $scope.producto.imagen = item.imagen;
    $scope.producto.nombre = item.nombre;
    $scope.producto.cantidad = item.cantidad;
    $scope.openModal();
  };
})  

.controller('CompraFireCtrl', function($scope, Productos, $firebaseArray, Firebase, ENV, $ionicModal, $localstorage){
  
  $scope.canScan = true;
  try{
    var a = cordova;
    a = 1;
  }catch(e){
    $scope.canScan = false;
  }

  var config = $localstorage.getObject('snapConfig');
  
  var carrito = new Firebase(ENV.firebaseAPI+'carritos');

  $scope.carrito = $firebaseArray(carrito.child(config.cedula));

  $scope.total = 0;

  carrito.child(config.cedula).on('value', function(snap){
    var total = 0;
    snap.forEach(function(a){
       total += parseFloat(a.val().precio) * parseInt(a.val().cantidad);
    });
    $scope.total = total;
  });

  $scope.itemElegido = {};

  $ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function(item) {
    $scope.modal.show();
    $scope.itemElegido = item;
  };

  $scope.closeModal = function() {
    $scope.modal.hide();

    if ($scope.itemElegido.cantidad > 0){
      carrito.child(config.cedula+'/'+$scope.itemElegido.$id).update({cantidad: $scope.itemElegido.cantidad});  
    }else{
      carrito.child(config.cedula+'/'+$scope.itemElegido.$id).remove();
    }
    
  };

  $scope.sumar = function(cant){
    $scope.itemElegido.cantidad += cant;
  };

  $scope.buscar = function(){
    Productos.get({productoCodigo:$scope.producto.codigo}, function(res){
      if (res){
        $scope.carrito.$add({
          cantidad:1, 
          codigo:res.id,
          nombre:res.nombre,
          precio: res.precio
        });
      }
    });
  };

  $scope.scan = function(){
    cordova.plugins.barcodeScanner.scan(function(data){
      Productos.get({productoCodigo:data.text}, function(res){
        if (res){
          $scope.carrito.$add({
            cantidad:1, 
            codigo:res.id,
            nombre:res.nombre,
            precio: res.precio
          });
        }
      });  
    });
  };
})

.controller('CompraFire2Ctrl', function($scope, $localstorage, Firebase, $firebaseArray, Productos, ENV, $ionicLoading,$ionicModal, $ionicPopup, Carritos, $resource){
  var config = $localstorage.getObject('snapConfig');
  var tienda = $localstorage.getObject('tienda');
  var apiEndpoint = $localstorage.get('apiEndpoint');

  $scope.title = tienda.nombre;

  var carrito = new Firebase(ENV.firebaseAPI+'carts/'+tienda.codigo );
  var productos = $localstorage.getObject('productos');
  $scope.ultimoPago = {};

  $scope.checkout = {tipoPago:{}};
  $scope.tiposPago = [
    {id:1, name:'Tarjeta de Crédito'},
    {id:2, name:'Tarjeta de Débito'},
    {id:3, name:'Pago en caja'}
  ];

  $scope.paso = 1;
  
  $scope.codigoManual = false;
  
  $scope.goinManual = function(){
    return $scope.codigoManual === true;
  };

  $scope.onHold = function(){
    $scope.codigoManual = !$scope.codigoManual;
  };

  $ionicModal.fromTemplateUrl('modal-checkout.html', {
    id:2,
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalCheckout = modal;
  });
  $scope.checkoutOpenModal = function() {
    $scope.modalCheckout.show().then(function(){
      
    });
  };
  $scope.checkoutCloseModal = function() {
    $scope.modalCheckout.hide().then(function(){
      
    });
  };

  $scope.btnCheckout = function(){
    config = $localstorage.getObject('snapConfig');

    Carritos.save({'productos':$scope.carrito, 'config':config, 'checkoutVia':$scope.checkout.tipoPago.id})
    .$promise.then(function(res){
      carrito.child(config.cedula).remove();
      $localstorage.setObject('ultimoPago', res.imagen);
      $scope.ultimoPago = res.imagen;
    });
  };
  
  $scope.showAlert = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: 'No hay productos cargados'
    });
    alertPopup.then(function() {
      console.log('Thank you for not eating my delicious ice cream cone');
    });
  };
  
  if (Object.keys(productos).length === 0){
    $scope.showAlert();
  }

  $scope.itemScanned = {};
  $scope.producto = {};
  $scope.total = 0;
  $scope.totalBI = 0;
  $scope.baseIva = 0;
  $scope.baseSinIva = 0;
  $scope.classBudget = 'positive';
  $scope.carrito = $firebaseArray(carrito.child(config.cedula));
  $scope.mode = '';

  $scope.isAdd = function(){
    return $scope.mode === 'add';
  };

  $scope.showAlert = function() {
    var alertPopup = '';
    alertPopup = $ionicPopup.alert({
      title: 'Aviso',
      template: 'El límite de ('+config.presupuesto+') ha sido excedido'
    });
  };

  carrito.child(config.cedula).on('value', function(snap){
    var total = 0;
    var totalBI = 0;
    var baseIva = 0;
    var baseSinIva = 0;
    var cantidadProductos = 0;
    snap.forEach(function(a){
      cantidadProductos += parseInt(a.val().qty);
      total += parseFloat(a.val().precio_imp) * parseFloat(a.val().qty);      
      totalBI += parseFloat(a.val().precio) * parseFloat(a.val().qty);
      if (a.val().tasa > 0){
        baseIva += parseFloat(a.val().precio) * parseFloat(a.val().qty);
      }else{
        baseSinIva += parseFloat(a.val().precio) * parseFloat(a.val().qty);
      }
    });
    $scope.cantidad = cantidadProductos;

    if (total > config.presupuesto){
      $scope.classBudget = 'assertive';
      $scope.showAlert();
    }else{
      $scope.classBudget = 'positive';
    }
    $scope.total = total;
    $scope.totalBI = totalBI;
    $scope.baseIva = baseIva;
    $scope.baseSinIva = baseSinIva;
  });

  $ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show().then(function(){
      $scope.codigoManual = false;
    });
  };
  $scope.closeModal = function() {
    $scope.modal.hide().then(function(){
      $scope.itemScanned = {};
    });
  };

  $scope.sumar = function(cant){
    if ($scope.itemScanned.qty + cant > 0){
      $scope.itemScanned.qty += cant;  
    }
  };

  $scope.agregar = function(){
    $scope.carrito.$add($scope.itemScanned);
    $scope.closeModal();
  };

  $scope.update = function(){
    carrito.child(config.cedula)
    .child($scope.itemScanned.$id)
    .update({qty:$scope.itemScanned.qty});
    $scope.closeModal();
  };

  $scope.edit = function(item){
    $scope.itemScanned = item;
    $scope.openModal();
    $scope.mode = 'edit';
  };
  
  $scope.scan = function(){
    if ($scope.codigoManual===true){
      $scope.getProducto({text:$scope.producto.codigo});
    }else{
      cordova.plugins.barcodeScanner.scan(function(data){
        $scope.getProducto(data);
      });  
    }
  };

  $scope.getProducto = function(data){
    var res = productos[data.text];
    if (res !== undefined){
      $scope.mode = 'add';
      res.qty = 1;
      if (res.es_exento === ''){
        res.es_exento = '+IVA';
      }
      $scope.itemScanned = res;
      $scope.openModal();  
    }else{
      $scope.msg = 'No tiene los productos cargados de la tienda';
    }
  };

  $scope.scan2 = function(){
    if ($scope.codigoManual === true){
      $scope.getProducto2({text:$scope.producto.codigo});
    }else{
      cordova.plugins.barcodeScanner.scan(function(data){
        $scope.getProducto2(data);
      });  
    }
  };

  var ProductosAPI = $resource(apiEndpoint + 'producto/:tiendaId/:productoCodigo', {tiendaId:tienda.id,productoCodigo:'@productoCodigo'});

  $scope.getProducto2 = function(data){
    var res = {};  
    $ionicLoading.show();
    ProductosAPI.get({productoCodigo:data.text}).$promise
    .then(function(data){
      var ros = data;
      res.nombre = ros.nombre;
      res.codigo = ros.codigo;
      res.precio_imp = ros.precio_imp;
      res.precio = ros.precio;
      res.descripcion = ros.descripcion;
      res.tasa = ros.tasa;
      res.es_exento = ros.es_exento;
      res.imagen = ros.imagen;
    })
    .catch(function(err){console.log(err);}).
    finally(function(){

      if (res !== undefined){
        $scope.mode = 'add';
        res.qty = 1;
        if (res.es_exento === ''){
          res.es_exento = '+IVA';
        }
        $scope.itemScanned = res;
        $scope.openModal();  
      }else{
        $scope.msg = 'No tiene los productos cargados de la tienda';
      }
      $ionicLoading.hide();

    });

    
  };

  $scope.procPago = function(){

  };

})

.controller('ConfigCtrl', function($scope, $localstorage){
  $scope.configuracion = $localstorage.getObject('snapConfig');

  $scope.guardar = function(){
    $localstorage.setObject('snapConfig', $scope.configuracion);
  };
})

.controller('AfiliacionesCtrl', function($scope, $localstorage, $ionicModal){
  $scope.afiliacion = {};

  $scope.afiliaciones = [
    {comercio:'Comercio 1', codigo:'00011738998',imagen:'../images/logo_tienda/emmeti.png'},
    {comercio:'Comercio 2', codigo:'000485089983711',imagen:'../images/logo_tienda/food.png'},
  ];

  $scope.comercios = [
    {comercio:'Comercio 1', codigo:'00011738998',imagen:'../images/logo_tienda/emmeti.png'},
    {comercio:'Comercio 2', codigo:'000485089983711',imagen:'../images/logo_tienda/food.png'},
  ];

  $scope.colors = [
    {name:'black', shade:'dark'},
    {name:'white', shade:'light', notAnOption: true},
    {name:'red', shade:'dark'},
    {name:'blue', shade:'dark', notAnOption: true},
    {name:'yellow', shade:'light', notAnOption: false}
  ];

  $ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function(afil) {
    $scope.modal.show();
    console.log(afil);
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

});
