'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'config', 'auth0', 'angular-jwt'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, authProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('home', {
    url: '/home',
    abstract: true,
    templateUrl: 'templates/login-primero.html',
    controller: 'LoginCtrl'
  })

  .state('home.login', {
    url: '/home-login',
    cache:false,
    views:{
      'menuContent': {
        templateUrl: 'templates/login-primero.html',
        controller: 'LoginCtrl'
      }
    }
  })
  .state('home.logout', {
    url: '/home-logout',
    cache:false,
    views:{
      'menuContent': {
        templateUrl: 'templates/logout.html',
        controller: 'LogoutCtrl'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })

  .state('app.lista', {
    url: '/lista',
    views: {
      'menuContent': {
        templateUrl: 'templates/lista.html',
        controller: 'ListaCtrl'
      }
    },
    data: {
      requiresLogin: true
    }
  })

  .state('app.listafire', {
    url: '/listafire',
    views: {
      'menuContent': {
        templateUrl: 'templates/listafire.html',
        controller: 'ListafireCtrl'
      }
    }
  })

  .state('app.afiliaciones', {
    url: '/afiliaciones',
    views: {
      'menuContent': {
        templateUrl: 'templates/afiliaciones.html',
        controller: 'AfiliacionesCtrl'
      }
    }
  })

  .state('app.config', {
    url: '/config',
    views: {
      'menuContent': {
        templateUrl: 'templates/config.html',
        controller: 'ConfigCtrl'
      }
    }
  })

  .state('app.comprafire', {
    url: '/comprafire',
    views: {
      'menuContent': {
        templateUrl: 'templates/comprafire.html',
        controller: 'CompraFireCtrl'
      }
    }
  })

  .state('app.activartienda', {
    url: '/activartienda',
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/activartienda.html',
        controller: 'AppCtrl'
      }
    },
    data: {
      requiresLogin: true
    }
  })  

  .state('app.comprafire2', {
    url: '/comprafire2',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/comprafire2.html',
        controller: 'CompraFire2Ctrl'
      }
    },
    data: {
      requiresLogin: true
    }
  })
    .state('app.registro', {
    url: '/registro',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/registro.html',
        controller: 'CompraFire2Ctrl'
      }
    }
  })
  .state('app.respuestaqr', {
    url: '/respuestaqr',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/respuestaqr.html',
        controller: 'CompraFire2Ctrl'
      }
    }
  })
  .state('app.bienvenida', {
    url: '/bienvenida',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/bienvenida.html',
        controller: 'CompraFire2Ctrl'
      }
    }
  })
  .state('app.login-primero', {
    url: '/login-primero', 
    views: {
      'menuContent': {
        templateUrl: 'templates/login-primero.html',
        controller: 'LoginCtrl',
      }
    }
  })
  .state('app.logout', {
    url: '/logout', 
    cache:false,
    views: {
      'menuContent': {
        templateUrl: 'templates/logout.html',
        controller: 'LogoutCtrl',
      }
    }
  })

  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/activartienda');

  authProvider.init({
    domain: 'israelsimmons.auth0.com',
    clientID: 'I4moN5wO9tApM71eWTpRBOIuJM3D1loY',
    loginState: 'home.login',
    callbackUrl: location.href,
    dict:'es'
  });

});


